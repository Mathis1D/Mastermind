import random, sys


#Liste des couleurs à choisir
couleurs = ['rouge', 'bleu', 'vert', 'jaune','noir', 'orange']

#Initiales des couleurs à choisir 
initiales_couleurs = []
for couleur in couleurs:
    initiales_couleurs.append(couleur[0]) 

#Longueur fixée pour la combinaison de couleurs
longueur_combinaison = 4

#Nombre maximal de tentatives 
nb_max_tentatives = 10

#Cette fonction permet d'afficher les différentes couleurs qui peuvent être choisies pour composer la combinaison
def afficher_les_couleurs_à_choisir():
    print('Les couleurs proposés sont:')
    for couleur in couleurs:
        print('-', couleur)

#Cette fonction permet de créer la combinaison secrète qui va être recherchée par le joueur
def creer_combinaison_secrete():
    combinaison_secrete = ''
    for i in range(0,longueur_combinaison):
        index = random.randint(0,len(initiales_couleurs)-1)
        combinaison_secrete = combinaison_secrete + initiales_couleurs[index]
    return combinaison_secrete

#Cette fonction permet de comparer la combinaison du joueur et celle crée par l'ordinateur et renvoie le nombre de couleur bien placée et le nombre de couleurs mal placées
def comparer_combinaison(combinaison_entree,combinaison_secrete):

#La premiére étape nous permet de éfinir les différentes varables
#qui vont entrer en jeu dans cette fonction
    nb_couleur_bien_placees = 0
    nb_couleur_mal_placees = 0
    initiales_couleurs_entrees = list(combinaison_entree)
    initiales_couleurs_secretes = list(combinaison_secrete)

#Ici nous vérifions une par une les lettres de la combinaison définie et celle que le joueur à soumis à la vérification
#et lorsque 2 correspondent et sont placés au même endroit on ajoute 1 à notre variable et on les remplace par 'déja comptabilisé'   
    for i in range(0,len(initiales_couleurs_entrees)):
        if initiales_couleurs_entrees[i] == initiales_couleurs_secretes[i]:
            nb_couleur_bien_placees += 1
            initiales_couleurs_entrees[i] = 'déja comptabilisé'
            initiales_couleurs_secretes[i] = 'deja comptabilisé'

#Ici nous faisons la même chose que précédemment seulement nous vérifions juste si 2 couleurs correspondent
#et non si elles sont placés au même endroit et aussi si la couleur trouvé n'est pas 'déja comptabilisé' ce qui signifierait qu'elle aurait été pris en compte précédemment
#puis si ces 2 facteurs sont réunis nous transformons cette donnée en 'déja comptabiliseé' et nous ajoutons 1 à une autre variable 
    for i in range(0,len(initiales_couleurs_entrees)):
        if initiales_couleurs_entrees[i] in initiales_couleurs_secretes:
            if initiales_couleurs_entrees[i] != 'déja comptabilisé':
                nb_couleur_mal_placees += 1
                couleur_actuelle = initiales_couleurs_entrees[i]
                j=initiales_couleurs_secretes.index(couleur_actuelle)
                initiales_couleurs_secretes[j] = 'déja comptabilisé'

#Nous retournons ensuite la valeur des 2 variables nous indiquant le nombre de couleur bien placé et le nombre de couleurs mal placées 
    return [nb_couleur_bien_placees, nb_couleur_mal_placees]


#Cette fonction nous permet de démander une combinaison au joueur et de vérifier si cette combinaison à les caractérisique prévu 
def demander_combinaison_au_joueur():

#On crée une variable booléenne ayant pour valeur false et on demande au joueur de saisir sa combinaison 
    invalide = False
    combinaison_joueur = input("Entrer votre combinaison: ")

#Lors de cette étape nous vérifions que la longueur de la combinaison saisie par le joueur est bien la même que celle qui à
#été définie et donc la même que celle crée par l'ordinateur si ce n'est pas le cas on renvoie un message d'erreur et modifie la variable invalide en true
    if len(combinaison_joueur) != longueur_combinaison:
       print("Longueur incorrecte")
       invalide = True

#Ici nous vérifions que toute les lettres entrées par le joueur sont définies dans notre liste de couleur à choisir si ce n'est pas le cas
# on renvoie un message d'erreur indiquant quelle lettre n'existe pas et on modifie la  variable invalide en true 
    for lettre in combinaison_joueur:
        if lettre not in initiales_couleurs:
            print ("la couleur", lettre, "n'existe pas")
            invalide = True

#Si la variable est égal à True cela signifie qu'il y'a une erreur on demande alors à notre programme de retourner erreur sinon on prend en compte sa combinaison     
    if invalide == True :
        return "Erreur"
    else:
        return combinaison_joueur
    
#Programme principal

#On commence par montrer au joueur les choix de couleur dont il dispose 
afficher_les_couleurs_à_choisir()

#Ensuite on génere la combinaison de l'ordinateur
combinaison_ordi = creer_combinaison_secrete()

for i in range(0,nb_max_tentatives):
    combinaison_user = "Erreur"
    while combinaison_user == "Erreur":
        combinaison_user = demander_combinaison_au_joueur()
    valeurs_comparees = comparer_combinaison(combinaison_user, combinaison_ordi) 
    if valeurs_comparees[0] == longueur_combinaison:
        print("Bravo, vous avez gagné")
        break
    else:
        print("Vous avez",valeurs_comparees[0],"couleurs bien placés et",valeurs_comparees[1],"couleurs mal placés")
        
        
