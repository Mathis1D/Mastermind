import matplotlib.pyplot as plt 
import numpy as np 
from random import random


fig = plt.figure()
d = 1 
plt.axis([-d, d, -d, d])
nbballe=20
balles = {}

for i in range(1, nbballe+1):
    balles[f"balle{i}"] = {'x' : random()-0.5, 'y' : random()-0.5, 'vx': (random()-0.5)/10, 'vy' : (random()-0.5)/10, 'Etat' : 'bo'}

balles["balle1"]["etat"] = 'ro'


def choc(b1, b2):
    x1, x2 , y1, y2 = b1['x'], b2['x'], b1['y'], b2['y']
    vx1, vx2, vy1, vy2 = b1['vx'], b2['vx'], b1['vy'], b2['vy']
    dx = x2 - x1
    dy = y2 - y1
    dist = np.sqrt(dx**2 + dy**2)
    if dist <= 2.1*rayon:
        nx, ny = dx/dist, dy/dist # vecteur normal unitaire 
        tx , ty = -ny, nx # vecteur tangente unitaire
        vn1 = (vx1*nx + vy1*ny) # projection 1 normale
        vt1 = (vx1*tx + vy1*ty) # projection 1 tangentielle 
        vn2 = (vx2*nx + vy2*ny) # projection 2 normale
        vt2 = (vx2*tx + vy2*ty) # projection 2 tangentielle
        vnP1, vnP2 = vn2, vn1 # les vecteurs normaux sont échangés comme un choc 1D
        vtP1, vtP2 = vt1, vt2 # vecteurs tan inchangés
        b1['vx'] = vnP1*nx + vt1*tx # v = vn + vt
        b1['vy'] = vnP1*ny + vt1*ty
        b2['vx'] = vnP2*nx + vt2*tx
        b2['vy'] = vnP2*ny + vt2*ty
        if int(b1['etat'] == 'ro') ^ int(b2['etat'] == 'ro'): #contamination
            b1['etat'] = 'ro'
            b2['etat'] = 'ro'


def mur(b):
    if abs(b['x'] + b['vx']) > d - rayon: #test en considérant la vitesse inchangée si la balle sortirai du graphique à droite ou à gauche
        b['vx'] = -b['vx']
    if abs(b['y'] + b['vy']) > d - rayon: #test en considérant la vitesse inchangée si la balle sortirai du graphique en haut ou en bas
        b['vy'] = -b['vy']


def mouvement(b): # actualise la position de la balle à la frame suivante
    b['x'] = b['x'] + b['vx']
    b['y'] = b['y'] + b['vy']


Numero_de_frame = 0

def next_frame():
    global Numero_de_frame
    Numero_de_frame += 1
    plt.clf()
    plt.axis([-d,d,-d,d])
    for i in range (1,nbballe+1)
        for j in range (i+1,nbballe+1)   
            choc(balles[f"balle{i}"],balles[f"balle{j}"])
    plt.title(f"Nombre de frame : {Numero_de_frame}")


def continu():
    liste_sains = []
    for i in range(1, nombre+1):
        if balle{i}['etat'] == 'bo':
            liste_sains.append(balle{i})") #ajoute à la liste des personnes saines toutes les balles bleues
    return bool(len(liste_sains)) #renvoie True s'il reste des personnes saines, et False si il n'y en a plus


next_loop = True
plt.pause(1)
while next_loop:
    next_frame() #actualise le graphique
    plt.pause(0.001) #fait une pause
    next_loop = continu() #permet de quitter la boucle while (donc d'arrêter le programme) une fois qu'il ne reste plus personne de sain


plt.show() #affiche l'animation créée