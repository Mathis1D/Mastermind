### Mastermind en Python
## Comment y jouer ?

Pour jouer au Mastermind à l'aide de ce programme python il faut disposer d'un inteprteur et donc de python si ce n'est pas le cas vous pouvez télécharger python grâce à ce lien:
[Python](https://www.python.org/downloads/)

Ensuite lancer le programme qui se trouve dans ce répertoire Gitlab ou [ici](https://gitlab.com/Mathis1D/Mastermind/-/blob/master/python.py) et
que vous l'aurez téléchargé ouvrez le dans python avec open qui se trouve dans file en haut à gauche de Python. Une fois que cela est fait vous pouvez le lancer en appuyant sur la commande permettant d'éxecuter le script si c'est dans python diréctement il s'agira de 
run puis run module ou F5. Une fois cela effectué il vous suffit de jouer lorsque l'on vous demande d'entrer votre combinaison il faut rentrer les initiales des couleurs que vous 
souhaitez soumettre au test pour savoir si vous avez gagné. Une fois vos 10 essais terminés le jeu se finit si vous souhaitez retenter votre chance avec une nouvelle combinaison 
il vous suffit de relancer le programme.

# Pour ceux qui s'y connaissent un peu en python
Le script à été créé pour qu'il puisse être modifiable si vous le souhaitez et que vous vous y connaissez un peu en python car dans le code il faut modifier seulement les 
paramétres modifiables par exemple si l'on souhaite modifier les choix pour les couleurs c'est possible on peut en ajouter ou en enlever en changeant la liste couleurs
ATTENTION à 2 choses, première chose bien utiliser la syntaxe des listes, deuxième chose dans ce programme il n'est pas possible de mettre 2 couleurs ayant la même première
lettre alors prudence. On peut aussi modifier le nombre de couleurs omposant la combinaison de l'ordinateur et donc les propositions pour trouver la combinaison mystère pour
cela change la valeur liée à la varible longueur_combinaison. Si l'on veut rendre le jeu plus dur ou plus simple l'on peut aussi changer le nombre d'essai pour ça modifier la valeur
de nb_max_tentatives.